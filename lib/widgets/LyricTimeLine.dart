import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:lyric_editor/models/TimerType.dart';
import "../pages/values/GlobalValues.dart" as GlobalValues;

class LyricTimeLine extends StatefulWidget {
  Duration time;
  int index;
  TimerType timerType;

  String getTime() {
    return time.toString();
  }

  LyricTimeLine({Key key, this.time, this.index, this.timerType})
      : super(key: key);
  @override
  _LyricTimeLineState createState() => _LyricTimeLineState();
}

class _LyricTimeLineState extends State<LyricTimeLine> {
  height(double d) {
    return MediaQuery.of(context).size.height * d;
  }

  width(double d) {
    return MediaQuery.of(context).size.width * d;
  }

  pullForward(int d) {
    setState(() {
      widget.time += Duration(milliseconds: d);
    });
    GlobalValues.jsonFormatedLabel[widget.index]
            [widget.timerType == TimerType.start ? "start" : "end"] =
        widget.time.toString();
  }

  pullBack(int d) {
    setState(() {
      widget.time -= Duration(milliseconds: d);
    });
    GlobalValues.jsonFormatedLabel[widget.index]
            [widget.timerType == TimerType.start ? "start" : "end"] =
        widget.time.toString();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width(0.35),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          GestureDetector(
              onTap: () {
                pullBack(500);
              },
              child: Icon(Icons.fast_rewind, size: 30)),
          Text(
            widget.time == null ? "00.0000" : widget.time.toString(),
            style: TextStyle(fontSize: 12),
          ),
          GestureDetector(
              onTap: () {
                pullForward(500);
              },
              child: Icon(
                Icons.fast_forward,
                size: 30,
              )),
        ],
      ),
    );
  }
}
