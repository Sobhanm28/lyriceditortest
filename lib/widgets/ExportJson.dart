import 'dart:convert';
import 'dart:io';

import 'package:path_provider/path_provider.dart';

import '../pages/values/GlobalValues.dart' as GlobalValues;

class ExportJson {
  generateJson(String id, String title, String artist, String album,
      String duration, String authors, String lyrics) {
    Map json = {
      "id": id,
      "file": "$artist - $title",
      "title": title,
      "album": album,
      "length": duration,
      "artists": artist,
      "authors": authors,
      "lyrics": {"raw": lyrics, "formatted": GlobalValues.jsonFormatedLabel}
    };

    createFile(json,artist,title);
  
  }

  generateFormatedLabel(String start, String end, String text) {
    GlobalValues.jsonFormatedLabel.add({
      "start": "$start",
      "end": "$end",
      "text": "$text",
      "words": GlobalValues.jsonWordsLabel
    });

    print(
        "============== ${GlobalValues.jsonFormatedLabel} ===================");
  }

  generateWordLabel(String text, String time) {
    GlobalValues.jsonWordsLabel.add({"text": text, "time": time});
    print(GlobalValues.jsonWordsLabel);
  }

  createFile(Map map,String artist,String title) async{
    var dir=await getExternalStorageDirectory();
    Directory createDir=Directory("${dir.path}/lyric_editor"); 
    createDir.create();
    File file=File("${createDir.path}/$artist - $title.json");
    file.create();
    file.writeAsStringSync(json.encode(map));
  }
}
