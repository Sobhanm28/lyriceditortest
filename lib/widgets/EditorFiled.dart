import 'package:audiotagger/audiotagger.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import "../pages/LyricsView.dart";
import '../pages/values/GlobalValues.dart' as GlobalValues;

class EditorFiled extends StatefulWidget {
  final String artist;
  final String title;
  final String lyric;
  final String musicPath;
  final String album;

  const EditorFiled(
      {Key key, this.artist, this.title, this.lyric, this.musicPath,this.album})
      : super(key: key);
  @override
  _EditorFiledState createState() => _EditorFiledState();
}

class _EditorFiledState extends State<EditorFiled> {
  TextEditingController artistController;
  TextEditingController titleController;
  TextEditingController lyricController;
  TextEditingController albumController;

  List lyricLines = [];
  

  @override
  void initState() {
    super.initState();
    initController();
  }

  initController() {
    artistController = TextEditingController();
    titleController = TextEditingController();
    lyricController = TextEditingController();
    albumController=TextEditingController();

    artistController.text = widget.artist;
    titleController.text = widget.title;
    lyricController.text = widget.lyric;
    albumController.text=widget.album;
  }

  @override
  void dispose() {
    super.dispose();
    print("dispose EditorFileds");
  }

  filedItem(TextEditingController controller, String label, int lines,
      TextDirection direction) {
    return TextField(
      controller: controller,
      decoration: InputDecoration(labelText: label),
      maxLines: lines,
      textDirection: direction,
      textAlign: direction==TextDirection.ltr?TextAlign.left:TextAlign.right,
    );
  }

  height(double d) {
    return MediaQuery.of(context).size.height * d;
  }

  setTags(String artist, String title, String lyrics, String musicPath) async {
    var tags = <String, String>{
      "artist": artist,
      "title": title,
      "lyrics": lyrics,
    };

    Audiotagger audiotagger = Audiotagger();
    await audiotagger.writeTagsFromMap(path: musicPath, tags: tags);

    lyricLines=lyricController.text.split("\n");
  
    GlobalValues.currentLineIndex=-1;
    GlobalValues.lyricLineStatus.updateAll((key,value)=>false);
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => LyricsView(
                  musicPath: widget.musicPath,
                  lyricLines: lyricLines,
                  lyric: lyricController.text,
                  artist: widget.artist,
                  title: widget.title,
                  album: widget.album,
                )));
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        height: height(0.9),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            filedItem(artistController, "Artist", 1, TextDirection.ltr),
            filedItem(titleController, "Title", 1, TextDirection.ltr),
            filedItem(albumController, "Album", 1, TextDirection.ltr),
            filedItem(lyricController, "Lyric", 10, TextDirection.rtl),
            FlatButton(
              onPressed: () {
                print(lyricLines);
            

                setTags(artistController.text, titleController.text,
                    lyricController.text, widget.musicPath);
              },
              child: Text(
                "Next",
                style: TextStyle(color: Colors.white),
              ),
              color: Colors.blue,
            )
          ],
        ),
      ),
    );
  }
}
