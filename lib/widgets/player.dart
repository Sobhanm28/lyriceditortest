import 'dart:convert';

import 'package:audioplayers_with_rate/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import '../pages/values/GlobalValues.dart' as GlobalValues;
import "package:crypto/crypto.dart";

class Player {
  final String musicPath;
  final ValueChanged<void> refresh;
  double maxSlider;
  double vlaueSlider;
  AudioPlayer audioPlayer = AudioPlayer();
  Duration position;
  Player(this.musicPath, this.refresh);

  width(double d, BuildContext context) {
    return MediaQuery.of(context).size.width * d;
  }

  height(double d, BuildContext context) {
    return MediaQuery.of(context).size.height * d;
  }

  play() async {
    await audioPlayer.play(musicPath);
    GlobalValues.isPlaying = true;
    getMusicDuration();
    getPositionMusic();
  }

  stop() {
    audioPlayer.stop();
    GlobalValues.isPlaying = false;
    refresh(null);
  }

  pause() {
    audioPlayer.pause();
    GlobalValues.isPlaying = false;
    refresh(null);
  }

  resume() {
    audioPlayer.resume();
    GlobalValues.isPlaying = true;
    refresh(null);
  }

  getMusicDuration() {
    audioPlayer.durationHandler = (Duration duration) {
      maxSlider = duration.inMilliseconds / 10;
      GlobalValues.musicDuration=duration;
      print(maxSlider);
    };
  }

  getPositionMusic() {
    audioPlayer.positionHandler = (Duration p) {
      refresh(null);
      GlobalValues.position = p;
      position = p;
      vlaueSlider = p.inMilliseconds / 10;
      print(p.inMilliseconds);
    };
  }

  positionSlider() {
    return Slider(
        min: 0,
        max: maxSlider,
        value: vlaueSlider,
        activeColor: Colors.blue,
        onChanged: (v) async {});
  }

  speedSlider(BuildContext context) {
    return Positioned(
      bottom: 0,
      child: Container(
        width: width(1, context),
        height: height(0.07, context),
        color: Colors.amber[300],
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Text("slow"),
            Slider(
                min: 0.5,
                max: 1.5,
                value: GlobalValues.speedSliderValue,
                divisions: 10,
                onChanged: (v) {
                  GlobalValues.speedSliderValue = v;
                  audioPlayer.setRate(v);
                  refresh(null);
                }),
            Text("fast"),
          ],
        ),
      ),
    );
  }

  musicPosition() {
    return Text(
      GlobalValues.position == null
          ? "00:00:00"
          : GlobalValues.position.toString().split(".").first,
      style: TextStyle(fontSize: 20),
    );
  }

  pullBackMusic() {
    return IconButton(
        icon: Icon(Icons.replay_5),
        onPressed: () async {
          // int pullback=position.inMilliseconds-5000;
          // print(Duration(milliseconds: pullback));
          //  await audioPlayer.seek(Duration(milliseconds: pullback));
          // refresh(null);
        });
  }

}
