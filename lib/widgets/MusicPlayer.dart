import 'package:audioplayers_with_rate/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:lyric_editor/widgets/player.dart';
import '../pages/values/GlobalValues.dart' as GlobalValues;

class MusicPlayer extends StatefulWidget {
  final Player audioPlayer;

  const MusicPlayer({Key key, this.audioPlayer}) : super(key: key);
  @override
  _MusicPlayerState createState() => _MusicPlayerState();
}

class _MusicPlayerState extends State<MusicPlayer> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            widget.audioPlayer.pullBackMusic(),
            IconButton(
                onPressed: () async {
                  GlobalValues.isPlaying
                      ? widget.audioPlayer.pause()
                      : widget.audioPlayer.resume();
                },
                icon: Icon(
                  GlobalValues.isPlaying ? Icons.pause : Icons.play_arrow,
                  size: 30,
                )),
            IconButton(icon: Icon(Icons.forward_5), onPressed: () {}),
          ],
        ),
        widget.audioPlayer.positionSlider(),
        IconButton(
          onPressed: () {
            widget.audioPlayer.stop();
          },
          icon: Icon(
            Icons.stop,
            size: 30,
          ),
        ),
        Center(
          child: widget.audioPlayer.musicPosition(),
        )
      ],
    );
  }
}
