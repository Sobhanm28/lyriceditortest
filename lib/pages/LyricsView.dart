import 'dart:convert';

import 'package:audioplayers_with_rate/audioplayers.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:file_picker/file_picker.dart';
import 'package:lyric_editor/models/LyricsLineModel.dart';
import 'package:lyric_editor/models/LyricsWordModel.dart';
import 'package:lyric_editor/widgets/ExportJson.dart';
import 'package:lyric_editor/widgets/MusicPlayer.dart';
import 'package:lyric_editor/widgets/player.dart';
import '../pages/values/GlobalValues.dart' as GlobalValues;
import "package:crypto/crypto.dart";

class LyricsView extends StatefulWidget {
  final String musicPath;
  final List lyricLines;
  final String lyric;
  final String title;
  final String artist;
  final String album;

  const LyricsView({Key key, this.musicPath, this.lyricLines, this.lyric, this.title, this.artist, this.album}) : super(key: key); 

  
  @override
  _LyricsViewState createState() => _LyricsViewState();
}

class _LyricsViewState extends State<LyricsView> {
  Player audioPlayer;
  Duration duration;
  int currentLineIndex;
  int currentWordIndex;
  List lyricWords;

  @override
  void initState() {
    super.initState();
    selectMusic();
  }

  selectMusic() async {
    audioPlayer = Player(widget.musicPath, refresh);
    duration = audioPlayer.getMusicDuration();
    audioPlayer.play();
  }

  refresh(_) {
    setState(() {});
  }

  @override
  void dispose() {
    super.dispose();
    GlobalValues.isPlaying ? audioPlayer.stop() : print("");
  }

  width(double d) {
    return MediaQuery.of(context).size.width * d;
  }

  height(double d) {
    return MediaQuery.of(context).size.height * d;
  }

    String convertTime2Md5(String time) {
    String res = md5.convert(utf8.encode(time)).toString();
    return res;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: <Widget>[
          Padding(
              padding: EdgeInsets.only(right: 10),
              child: IconButton(icon: Icon(Icons.save), onPressed: () {
                String id=convertTime2Md5("${DateTime.now().microsecondsSinceEpoch}");
                String duration=GlobalValues.musicDuration.toString();
                String authors="";
                ExportJson()..generateJson(id, widget.title, widget.artist, widget.album, duration, authors, widget.lyric);
                GlobalValues.jsonFormatedLabel=[];
              }))
        ],
        title: GestureDetector(
            onTap: () {
              print(GlobalValues.jsonFormatedLabel);
            },
            child: Text("LyricsView")),
      ),
      body: Stack(
        children: <Widget>[
          Column(
            children: <Widget>[
              Expanded(
                flex: 5,
                child: Center(
                  child: audioPlayer == null
                      ? Center(
                          child: CircularProgressIndicator(),
                        )
                      : MusicPlayer(
                          audioPlayer: audioPlayer,
                        ),
                ),
              ),
              Expanded(
                  flex: 8,
                  child: Container(
                    padding: EdgeInsets.only(bottom: height(0.07)),
                    child: ListView.builder(
                        itemCount: widget.lyricLines.length,
                        itemBuilder: (context, index) {
                          GlobalValues.lyricLineStatus
                              .putIfAbsent("line$index", () => false);
                          return Column(
                            children: <Widget>[
                              LyricsLineModel(
                                index: index,
                                lyricLines: widget.lyricLines,
                                refresh: refresh,
                              ),
                              // LyricsWordModel(
                              //   index: index,
                              //   lyricLine: widget.lyricLines[index],
                              //   refresh: refresh,
                              // )
                            ],
                          );
                        }),
                  ))
            ],
          ),
          audioPlayer.speedSlider(context)
        ],
      ),
    );
  }
}
