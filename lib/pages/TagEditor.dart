import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:lyric_editor/widgets/EditorFiled.dart';
import 'package:audiotagger/audiotagger.dart';
import 'package:lyric_editor/widgets/player.dart';

class TagEditor extends StatefulWidget {
  @override
  _TagEditorState createState() => _TagEditorState();
}

class _TagEditorState extends State<TagEditor> {
  String musicPath;
  String atrist, title, lyric,album;

  @override
  void initState() {
    super.initState();
    selectMusic();
  }

  selectMusic() async {
    musicPath = await FilePicker.getFilePath(type: FileType.audio);
    getMusicTags();
  }

  getMusicTags() async {
    Audiotagger audiotagger = Audiotagger();
    Map tags =
        await audiotagger.readTagsAsMap(path: musicPath, checkPermission: true);

    setState(() {
      atrist = tags["artist"];
      title = tags["title"];
      lyric = tags["lyrics"];
      album = tags["album"];
    });
  }

  refresh(_){
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: GestureDetector(
          onTap: (){
            
            Player audioPlayer=Player(musicPath, refresh);
            audioPlayer.stop();
          },
          child: Text("Tag Editor")),
      ),
      body: Container(
          padding: EdgeInsets.all(10),
       
          child: atrist == null
              ? Center(child: CircularProgressIndicator())
              : EditorFiled(
                  artist: atrist,
                  title: title,
                  lyric: lyric,
                  musicPath: musicPath,
                  album:album
                )
          ),
    );
  }
}
