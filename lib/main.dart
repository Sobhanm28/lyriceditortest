import 'package:flutter/material.dart';
import 'package:lyric_editor/pages/LyricsView.dart';
import 'package:lyric_editor/pages/TagEditor.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: TagEditor(),
    );
  }
}


