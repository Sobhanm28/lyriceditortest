import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:lyric_editor/models/TimerType.dart';
import 'package:lyric_editor/widgets/ExportJson.dart';
import 'package:lyric_editor/widgets/LyricTimeLine.dart';
import '../pages/values/GlobalValues.dart' as GlobalValues;

class LyricsLineModel extends StatefulWidget {
  final List lyricLines;
  final int index;
  final ValueChanged<void> refresh;

  const LyricsLineModel({Key key, this.lyricLines, this.index, this.refresh})
      : super(key: key);
  @override
  _LyricsLineModelState createState() => _LyricsLineModelState();
}



class _LyricsLineModelState extends State<LyricsLineModel> {
  List lyricWords;
  bool isHighlited = false;
  LyricTimeLine startTimeLine = LyricTimeLine();
  LyricTimeLine endTimeLine = LyricTimeLine();
  bool finishDetected = false;

  height(double d) {
    return MediaQuery.of(context).size.height * d;
  }

  width(double d) {
    return MediaQuery.of(context).size.width * d;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(15, 5, 15, 5),
      height:
          height(GlobalValues.currentLineIndex == widget.index ? 0.125 : 0.08),
      color: GlobalValues.currentLineIndex == widget.index
          ? Colors.grey[300]
          : Colors.white,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              GestureDetector(
                onTap: () {
                  lyricWords = widget.lyricLines[widget.index].split(" ");
                  GlobalValues.currentLineIndex = widget.index;

                  widget.refresh(null);
                },
                child: Icon(GlobalValues.currentLineIndex == widget.index
                    ? Icons.keyboard_arrow_down
                    : Icons.keyboard_arrow_right),
              ),
              // GlobalValues.lyricLineStatus["line${widget.index}"] == false
              //     ? Container()
              //     : Icon(
              //         Icons.done,
              //         color: Colors.green,
              //       ),

              Row(
                children: <Widget>[
                  GestureDetector(
                      onTap: () {
                        setState(() {
                          isHighlited = !isHighlited;
                        });

                        if (isHighlited) {
                          startTimeLine = LyricTimeLine(
                            time: GlobalValues.position,
                            index: widget.index,
                            timerType: TimerType.start,
                          );
                        } else {
                          endTimeLine = LyricTimeLine(
                            time: GlobalValues.position,
                            index: widget.index,
                            timerType: TimerType.end,
                          );

                          finishDetected = true;
                          ExportJson()
                            ..generateFormatedLabel(
                                startTimeLine.getTime(),
                                endTimeLine.getTime(),
                                widget.lyricLines[widget.index]);
                        }
                      },
                      child: Text(
                        widget.lyricLines[widget.index],
                        textDirection: TextDirection.rtl,
                        style: TextStyle(
                            color: isHighlited ? Colors.red : Colors.black),
                      )),
                  Text("   "),
                  finishDetected
                      ? Icon(
                          Icons.done,
                          color: Colors.green,
                        )
                      : Container(),
                ],
              )
            ],
          ),
          Center(
            child: GlobalValues.currentLineIndex == widget.index
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      endTimeLine,
                      startTimeLine,
                    ],
                  )
                : Container(),
          )
        ],
      ),
    );
  }
}
