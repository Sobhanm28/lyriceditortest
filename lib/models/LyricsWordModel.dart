import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:lyric_editor/widgets/ExportJson.dart';
import "../pages/values/GlobalValues.dart" as GlobalValues;

class LyricsWordModel extends StatefulWidget {
  final int index;
  final String lyricLine;
  final ValueChanged<void> refresh;

  const LyricsWordModel({Key key, this.index, this.lyricLine, this.refresh})
      : super(key: key);
  @override
  _LyricsWordModelState createState() => _LyricsWordModelState();
}

class _LyricsWordModelState extends State<LyricsWordModel> {
  List lyricWords;
  String time = "";
  Map times = {};
  Map colorWords = {};
  List words = [];
  width(double d) {
    return MediaQuery.of(context).size.width * d;
  }

  height(double d) {
    return MediaQuery.of(context).size.height * d;
  }

  @override
  void initState() {
    super.initState();
    print("init lyrics word");
    lyricWords = widget.lyricLine.split(" ");

    Map formatted = {
      "start": "00:00.00",
      "end": "end",
      "text": "text",
      "words": [
        {"text": "text1", "time": "time"},
        {"text": "text1", "time": "time"},
        {"text": "text1", "time": "time"},
      ]
    };
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: lyricWords == null
          ? Container()
          : Container(
              padding: EdgeInsets.all(10),
              width: width(1),
              height: height(
                  GlobalValues.currentLineIndex == widget.index ? 0.15 : 0),
              color: Colors.blue,
              child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                              child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  textDirection: TextDirection.rtl,
                  children: List.generate(lyricWords.length, (i) {
                    times.putIfAbsent("time$i", () => "");
                    colorWords.putIfAbsent("color$i", () => Colors.white);
                    return GestureDetector(
                      onTap: () {
                        GlobalValues.currentWordIndex = i;
                        time = GlobalValues.position.toString();
                        times.update("time$i", (c) => time);
                        colorWords.update("color$i", (c) => Colors.black);
                        ExportJson()..generateWordLabel("${lyricWords[i]}", "${times["time$i"]}");
                        if (times.values.last != "") {
                          GlobalValues.lyricLineStatus["line${widget.index}"] =
                              true;
                          ExportJson()..generateFormatedLabel("${times.values.first}", "${times.values.last}", widget.lyricLine);
                          GlobalValues.jsonWordsLabel=[];
                          GlobalValues.currentLineIndex += 1;
                          print(words);
                        }
                        widget.refresh(null);
                      },
                      child: Container(
                        color: Colors.blue[400],
                        padding: EdgeInsets.all(5),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Text(
                              lyricWords[i],
                              style: TextStyle(color: colorWords["color$i"]),
                            ),
                            Text(times["time$i"])
                          ],
                        ),
                      ),
                    );
                  }),
                ),
              ),
            ),
    );
  }
}
